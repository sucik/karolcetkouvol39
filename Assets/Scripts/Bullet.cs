﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public float Range { get; set; }

    public GameObject From { get; set; }

    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
        if(Vector3.Distance(From.transform.position, transform.position) >= Range)
        {
            //DSestroy and spawn new Turret:
            

            GameObject newTurret = Instantiate<GameObject>(From);
            
            newTurret.transform.position = transform.position;
     
            Destroy(gameObject);

        }
        

	}


    void OnColliderEnter()
    {
        
    }
}
