﻿using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour {

    private float rotationMin = 15;
    private float rotationMax = 45;

    private float bulletRangeMin = 1;
    private float bulletRangeMax = 4;


    [SerializeField]
    private GameObject projectile;

	// Use this for initialization
	void Start () {



        StartCoroutine("RotateTurret",  2.0f);
    }
	
	// Update is called once per frame
	void Update () {


        
        
	}



    void RotateTurret()
    {
        Quaternion rotation = Quaternion.AngleAxis(Random.Range(rotationMin, rotationMax), Vector3.up);
        transform.rotation *= rotation;
        shootAProjectile();
    }

    private void shootAProjectile()
    {
        GameObject clone = Instantiate<GameObject>(projectile);

        Bullet bullet = clone.GetComponent<Bullet>();
        bullet.From = gameObject;
        bullet.Range = Random.Range(bulletRangeMin, bulletRangeMax);
        clone.GetComponent<Rigidbody>().velocity = transform.TransformDirection((transform.rotation* Vector3.forward) * 4);
    }

}
