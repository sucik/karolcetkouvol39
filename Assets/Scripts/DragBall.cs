﻿using UnityEngine;
using System.Collections;

public class DragBall : MonoBehaviour {


    private float factor = 0.5f;

    private bool ballhit = false;
    GameObject currentBall = null;

    // Update is called once per frame
    void Update()
    {


        Vector3 hitpos = Vector3.zero;

        


        

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.CompareTag("Ball") )
                {
                    hitpos = hit.point;
                    ballhit = true;
                    currentBall = hit.collider.gameObject;
                }
                else
                {
                    ballhit = false;
                }
            }
        }

        if (ballhit)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.CompareTag("DirectionPlane"))
                {
                    float strength = Vector3.Distance(hit.point, hitpos);
                    Vector3 forceVector = factor * (hit.point - hitpos);
                    currentBall.GetComponent<Rigidbody>().AddForce(forceVector);
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            ballhit = false;
        }


    }
}
